<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tatuador/show/{tatuador}', 'TatuadorController@show')->name('tatuador.show');
Route::get('/tatuador', 'TatuadorController@index')->name('tatuador');
Route::get('/tatuajes/show/{tatuaje}','TatuajeController@show')->name('tatuajes.show');
Route::get('/tatuajes', 'TatuajeController@index')->name('tatuajes');


Route::group(['middleware' => 'auth'], function(){
  Route::group(['middleware' => 'Admin'], function(){
    Route::resource('/user', 'UserController');
  });
  Route::resource('/tatuador', 'TatuadorController', ['except' => ['show', 'index']]);
  Route::get('/tatuador/{tatuador}/estilo/edit', 'TatuadorEstiloController@edit')
    ->name('tatuador.estilo.edit');
  Route::patch('/tatuador/{tatuador}/estilo/edit', 'TatuadorEstiloController@update')
    ->name('tatuador.estilo.update');
  Route::resource('tatuador.estilo', 'TatuadorEstiloController', ['except' => ['edit', 'update']]);
  Route::resource('/tatuajes','TatuajeController', ['except' => ['show', 'index']]);
});


