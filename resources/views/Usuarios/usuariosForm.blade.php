@extends('layouts.app')
@section('content')

<div class="row">
  <div class="col-md-10">
    <h2>
      Captura de nuevo usuario
    </h2>
  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

@if(!isset($user))
  {!! Form::open(['action' => 'UserController@store']) !!}
@else
  {!! Form::model($user, ['action' => ['UserController@update', $user->id], 'method' => 'patch']) !!}
@endif

<div class="form-group">
  <label for="name">Nombre:</label>
  {!! Form::text('name', null, ['placeholder' => 'Nombre del Usuario', 'class' => 'form-control']) !!}
</div>

<div class="form-group">
  <label for="email">Correo:</label>
  {!! Form::email('email', null, ['placeholder' => 'Correo del Usuario', 'class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
	<label for="typeUser">Tipo de usuario:</label>
	{!! Form::select('typeUser', [
   'Administrador' => 'Administrador',
   'Tatuador' => 'Tatuador']
   , null, ['class' => 'custom-select']) !!}
</div>

<div class="form-group">
  <label for="password">Contraseña:</label>
  <input type="password" name="password" placeholder="Introduzca una Contraseña" class="form-control" required>
</div>

<div class="form-group">
  <label for="password_confirmation">Confirmar Contraseña:</label>
  <input type="password" name="password_confirmation" placeholder="Confirme la Contraseña" class="form-control" required>
</div>

<button type="submit" class="btn btn-success">Aceptar</button>

{!! Form::close() !!}
</div>
</div>
@endsection
