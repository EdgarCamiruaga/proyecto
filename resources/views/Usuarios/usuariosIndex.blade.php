@extends('layouts.app')
@section('content')
@include('layouts.mensajes')
<div class="row">
  <div class="col-md-12">
    <h1>
      Listado de usuarios
    </h1>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Tipo de usuario</th>
        </tr>
      </thead>
      <tbody>
       @foreach($usuarios as $u)
        <tr>
          <td>
            <a href="{{ action('UserController@show', $u->id) }}"
              target="_new"> {{ $u->name }}</a>
          </td>
          <td>{{ $u->typeUser }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
