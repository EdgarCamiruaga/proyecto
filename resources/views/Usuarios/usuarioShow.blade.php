@extends('layouts.app')
@section('content')


<div class="row">
  <div class="col-md-12">
    <h1>
      Info de usuario
    </h1>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Correo</th>
          <th>Tipo de usuario</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>{{ $user->typeUser}}</td>
        </tr>
      </tbody>
    </table>

    <br>
    <div class="row">
        {!! Form::open(['action' => ['UserController@edit', $user], 'method' => 'get']) !!}
        {!! Form::submit('Editar usuario', ['class' => 'btn btn-success']) !!}
        {!! Form::close() !!}

        {!! Form::open(['action' => ['UserController@destroy', $user->id], 'method' => 'delete']) !!}
        {!! Form::submit('Borrar usuario', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}

    </div>
  </div>
</div>
@endsection
