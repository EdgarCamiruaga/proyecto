@extends('layouts.app')
@section('content')
<body background="black">
  <h1>
    Tatuador
  </h1>

  <div class="container">
    <div class="row">
      <div class="col-sm">
        <h2>{{ $tatuador->name }} </h2>
        </div>
        <div class="col-sm">
        <h2>{{ $tatuador->email }}</h2>
        </div>
        <div class="col-sm">
        <h2>{{ $tatuador->nick }}</h2>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Estilos</th>
              </tr>
            </thead>
            <tbody>
            @foreach($misEstilos as $estilo)
              @foreach($estilos as $e)
                @if($e->id == $estilo)
                  <tr>
                    <td>{{ $e->nombre }}</td>
                  </tr>
                @endif
              @endforeach
            @endforeach
            </tbody>
          </table>
          @if(!Auth::guest())
            <div class="btn-group" role="group">
              <div class="col">
                {!! Form::open(['route' => ['tatuador.estilo.edit', $tatuador->id], 'method' => 'get']) !!}
                {!! Form::submit('Agregar, modificar o eliminar estilos del tatuador', ['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
              </div>
              
              <div class="col">
                {!! Form::open(['action' => ['TatuadorController@edit', $tatuador], 'method' => 'get']) !!}
                {!! Form::submit('Editar tatuador', ['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
              </div>

              <div class="col">
                {!! Form::open(['action' => ['TatuadorController@destroy', $tatuador->id], 'method' => 'delete']) !!}
                {!! Form::submit('Borrar tatuador', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
              </div>
            </div>
          @endif
      </div>
    </div>
  </div>
</body>
@endsection
