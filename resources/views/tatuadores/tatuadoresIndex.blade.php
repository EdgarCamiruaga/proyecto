@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>
      Listado de tatuadores
    </h1>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="table">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Apodo</th>
        </tr>
      </thead>
      <tbody>
       @foreach($tatuadores as $t)
        <tr>
          <td>
            <a href="{{ action('TatuadorController@show', $t->id) }}"
              target="_new"> {{ $t->name }}</a>
          </td>
          <td>{{ $t->nick }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
