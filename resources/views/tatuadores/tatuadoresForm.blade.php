@extends('layouts.app')
@section('content')

<div class="row">
  <div class="col-md-10">
    <h2>
      Captura de nuevo Tatuador
    </h2>
  </div>
</div>

<div class="row">
  <div class="col-md-8 offset-md-2">

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  @if(!isset($tatuador))
    {!! Form::open(['action' => 'TatuadorController@store']) !!}
  @else
    {!! Form::model($tatuador, ['action' => ['TatuadorController@update', $tatuador->id], 'method' => 'patch']) !!}
  @endif
<div class="form-row align-items-center">
  <div class="col-auto">
    <div class="form-group">
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <label class="sr-only" for="name">Nombre:</label>

        </div>
        {!! Form::text('name', null, ['placeholder' => 'Nombre del Usuario', 'class' => 'form-control']) !!}
      </div>
    </div>
  </div>

  <div class="col-auto">
    <div class="form-group">
      <label class="sr-only" for="email">Correo:</label>
      {!! Form::email('email', null, ['placeholder' => 'Correo del Usuario', 'class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-auto">
    <div class="form-group">
      <label class="sr-only"for="name">Apodo:</label>
      {!! Form::text('nick', null, ['placeholder' => 'Apodo', 'class' => 'form-control']) !!}
    </div>
  </div>

    <div class="col-auto">
      <button type="submit" class="btn btn-success">Aceptar</button>
    </div>

{!! Form::close() !!}
</div>
@endsection
