@extends('layouts.app')
@section('content')


<div class="row">
  <div class="col-md-8">
    <div class="alert alert-info">Modificar estilos a: {{ $tatuador->name }}</div>

    @if($tatuador->estilos->count() > 0)
      {!! Form::open(['route' => ['tatuador.estilo.update', $tatuador->id], 'method' => 'PATCH'], ['class' => 'form']) !!}
    @else
      {!! Form::open(['route' => ['tatuador.estilo.store', $tatuador->id]], ['class' => 'form']) !!}
    @endif

        {!! Form::label('estilo_id', 'Selecciona los estilos', ['class' => 'form-label']) !!}

        <select name="estilo_id[]" class="form-control" multiple>
            @foreach($estilos as $key => $value)
              <option value="{{ $key+1 }}" {{ array_search($key+1, $misEstilos) !== false ? 'selected' : '' }}>{{ $value->nombre }}</option>
            @endforeach
        </select>


        {!! Form::submit('Aceptar', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
  </div>
</div>
@endsection
