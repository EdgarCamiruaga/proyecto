@extends('layouts.app')
@section('content')
    <div class="card-columns">
        @foreach($tatuajes as $tatuaje)
            <div class="card">
                <img class="card-img-top" src="{{ Storage::url($tatuaje->fs_name) }}    " alt="{{ $tatuaje->titulo }}">            
                <div class="card-body">
                    <h5 class="card-title">{{ $tatuaje->titulo }}</h5>
                    <p class="card-text">{{ $tatuaje->autorTatuaje }}</p>
                </div>
                <div class="card-footer">
                    @foreach($estilos as $estilo)
                        @if($estilo->id == $tatuaje->estilo_id)
                            <small class="text-muted">{{ $estilo->nombre }}</small>
                        @endif
                    @endforeach
                </div>  

                    @if(!Auth::guest())
                        {!! Form::open(['action' => ['TatuajeController@destroy', $tatuaje->id], 'method' => 'delete']) !!}
                        {!! Form::submit('Borrar tatuaje', ['class' => 'btn btn-primaryr']) !!}
                        {!! Form::close() !!}
                    @endif
            </div>
        @endforeach
    </div>
@endsection