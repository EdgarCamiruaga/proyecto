@extends('layouts.app')
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
@section('content')

<div class="row">

  <div class="col-md-12">
      {!! Form::open(['route' => ['tatuajes.store'], 'foto' => 'true', 'files' => 'true']) !!}
      <label for="name">Titulo:</label>
      {!! Form::text('titulo', null, ['placeholder' => 'Titulo del tatuaje', 'class' => 'form-control']) !!}

      <label for="estilo_id">Estilo de tatuaje:</label>
      <label for="tatuador_id">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      &nbsp;&nbsp;Tatuador:</label>
      <div class="row">
        <div class="col-md-4">
          <select name="estilo_id" class="custom-select" id="select-estilos">
            @foreach($estilos as $estilo)
              <option value="{{ $estilo->id }}">{{ $estilo->nombre }}</option>
            @endforeach
          </select>
        </div>

        <div class="col-md-4">
          <select name="tatuador_id" class="custom-select" id="select-tatuadores"></select>
        </div>
      </div>

      <br>
      <div class="custom-file">
        {!! Form::file('foto', ['class' => 'custom-file-input']) !!}
        <label class="custom-file-label" for="foto">Seleccionar foto</label>
      </dv>
      {!! Form::submit('Subir foto', ['class' => 'btn btn-success']) !!}
      {!! Form::close() !!}

  </div>
</div>

<script>
  $(function() {
    $('#select-estilos').on('change', onSelectEstiloChange);
    
  });
  function onSelectEstiloChange(){
    var estilo_id = $(this).val();
    
    
    $.get('/api/estilos/'+estilo_id+'/tatuadores', function(data){ 
      $('#select-tatuadores').empty();
      $.each(data, function(index, d){
        $('#select-tatuadores').append('<option value ="'+d.id+'">'+d.name+' ('+d.nick+' )</option>');
      });
    });

  }
</script> 

@endsection

