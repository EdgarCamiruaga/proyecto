Sistema para hacer publicos los trabajos de los tatuadores pertenecientes al estudio Solitoos Ink.

Instalación:
Clonar el proyecto: git clone https://EdgarCamiruaga@bitbucket.org/EdgarCamiruaga/proyecto.git
Instalar dependencias: comporser install
Crear base de datos para el sistema
Crear archivo .env cp .env.example .env
configurar .env
crear llave de acceso: php artisan key:generate
Crear migraciones: php artisan migrate
Correr Seed: php artisan db:seed

