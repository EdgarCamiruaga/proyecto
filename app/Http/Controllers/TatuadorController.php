<?php

namespace App\Http\Controllers;

use App\Tatuador;
use App\Estilo;
use Illuminate\Http\Request;

class TatuadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tatuadores = Tatuador::all();

        return view('tatuadores.tatuadoresIndex', compact('tatuadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tatuadores.tatuadoresForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request,[
        'name' => 'required|min:3',
        'email' => 'required|email',
        'nick' =>'required|min:2',
      ]);
      Tatuador::create([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'nick' => $request->input('nick'),
      ]);

      return redirect()->action('TatuadorController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tatuador  $tatuador
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $tatuador = Tatuador::find($id);
      $estilos = Estilo::all();
      $misEstilos = $tatuador->estilos->pluck('id')->toArray();

      return view('tatuadores.tatuadoresShow',compact('tatuador','misEstilos',
                  'estilos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tatuador  $tatuador
     * @return \Illuminate\Http\Response
     */
    public function edit(Tatuador $tatuador)
    {
      return view('tatuadores.tatuadoresForm',compact('tatuador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tatuador  $tatuador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tatuador $tatuador)
    {
      $this->validate($request,[
        'name' => 'required|min:3',
        'email' => 'required|email',
        'nick' =>'required|min:2',
      ]);
      $tatuador->name = $request->input('name');
      $tatuador->email = $request->input('email');
      $tatuador->nick = $request->input('nick');
      $tatuador->save();

      return redirect()->action('TatuadorController@show', compact('tatuador'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tatuador  $tatuador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tatuador $tatuador)
    {
        //
    }
}
