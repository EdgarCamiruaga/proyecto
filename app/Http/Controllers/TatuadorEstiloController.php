<?php

namespace App\Http\Controllers;
use App\Tatuador;
use App\Estilo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TatuadorEstiloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tatuador $tatuador)
    {
      $tatuador->estilos()->attach($request->input('estilo_id'));

      return redirect()->action('TatuadorController@show', compact('tatuador'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $tatuador = Tatuador::find($id);
      $estilos = Estilo::all();
      $misEstilos = $tatuador->estilos->pluck('id')->toArray();


      return view('tatuadores.tatuadorEstilosForm' ,compact('tatuador',
                                                    'misEstilos','estilos'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estilo $estilo, Tatuador $tatuador)
    {
      $estilos = $request->input('estilo_id');

      DB::table('estilo_tatuador')->where('tatuador_id', '=', $tatuador->id)->delete();
      for($i = 0 ; $i<count($estilos); $i++){
        DB::table('estilo_tatuador')->insert(
          ['estilo_id' => $estilos[$i], 'tatuador_id' => $tatuador->id]
        );
      }
      return redirect()->action('TatuadorController@show', compact('tatuador'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
