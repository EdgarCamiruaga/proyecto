<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $usuarios = User::all();
      return view('usuarios.usuariosIndex', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.usuariosForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'name' => 'required|min:3',
        'email' => 'required|email',
        'password' => 'required|min:3|confirmed'
      ]);
      User::create([
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'typeUser' => $request->input('typeUser'),
        'password' =>  bcrypt($request->input('password'))
      ]);
      return redirect()->action('UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find($id);
      return view('usuarios.usuarioShow',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('usuarios.usuariosForm',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $this->validate($request,[
        'name' => 'required|min:3',
        'email' => 'required|email',
        'password' => 'required|min:3|confirmed'
      ]);
      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->typeUser = $request->input('typeUser');
      $user->password = bcrypt($request->input('password'));
      $user->save();

      return redirect()->action('UserController@show', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
      $user->delete();
      return redirect()->action('UserController@index');
    }
}
