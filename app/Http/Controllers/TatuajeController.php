<?php

namespace App\Http\Controllers;

use App\Tatuaje;
use App\Estilo;
use App\Tatuador;
use Illuminate\Http\Request;
use DB;

class TatuajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function porEstilo($id){
        $tatuadores = DB::table('tatuadores')
            ->join('estilo_tatuador', 'tatuadores.id', '=', 'estilo_tatuador.tatuador_id')
            ->join('estilos', 'estilo_tatuador.estilo_id', '=', 'estilos.id')
            ->where('estilos.id', '=', $id)
            ->select('tatuadores.*')->get();
        return response()->json($tatuadores);
    }

    public function index()
    {   
        $tatuajes = DB::table('tatuajes')
        ->join('tatuadores', 'tatuadores.id', 'tatuajes.tatuador_id')
        ->select('tatuajes.id','tatuajes.fs_name', 'tatuajes.titulo', 'tatuajes.estilo_id', 'tatuadores.nick as autorTatuaje')
        ->get();
        $estilos = Estilo::all();
        return view('tatuajes.tatuajesIndex', compact('tatuajes', 'estilos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tatuadores = Tatuador::all();
        $estilos = Estilo::all();
        return view('Tatuajes.tatuajesForm', compact('tatuadores','estilos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'foto' => 'required|image'
        ]);
        $foto = $request->file('foto');
        if ($request->hasFile('foto')) { 
            $fs_name = $request->foto->store('public');
          
            $tatuaje = Tatuaje::create([
            'mime' => $foto->getMimeType(),
            'titulo' => $request->input('titulo'),
            'fs_name' => $fs_name,
            'estilo_id' => $request->input('estilo_id'),
            'tatuador_id' => $request->input('tatuador_id'),
            ]);
        }  
        return redirect()->action('TatuajeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tatuaje  $tatuaje
     * @return \Illuminate\Http\Response
     */
    public function show(Tatuaje $tatuaje)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tatuaje  $tatuaje
     * @return \Illuminate\Http\Response
     */
    public function edit(Tatuaje $tatuaje)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tatuaje  $tatuaje
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tatuaje $tatuaje)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tatuaje  $tatuaje
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tatuaje $tatuaje)
    {
        \Storage::delete($tatuaje->fs_name);
        $tatuaje->delete();
        return redirect()->action('TatuajeController@index');
    }
}
