<?php

namespace App\Http\Middleware;

use Closure;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = \Auth::user();
        if($usuario->typeUser != 'Administrador'){
            return redirect('/');
        }
        return $next($request);
    }
}
