<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tatuador extends Model
{
  public $timestamps = false;
  protected $table = 'tatuadores';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'nick',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'remember_token',
  ];

  public function estilos()
  {
    return $this->belongsToMany(Estilo::class, 'estilo_tatuador');
  }

  public function tatuajes(){
    return $this->hasMany(Tatuaje::class);
  }

}
