<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tatuaje extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'titulo' , 'mime', 'fs_name', 'estilo_id', 'tatuador_id',
    ]; 

    public function tatuador()
    {
        return $this->hasOne('App\Tatuador');
    }

    public function estilo(){
        return $this->hasOne(Estilo::class);
    }
}
