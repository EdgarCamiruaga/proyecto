<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estilo extends Model
{
  public $timestamps = false;


  public function tatuadores()
  {
    return $this->belongsToMany(Tatuador::class, 'estilo_tatuador');
  }

  public function tatuajes(){
    return $this->hasMany(Tatuaje::class);
  }
}
