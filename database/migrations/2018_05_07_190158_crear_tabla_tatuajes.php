<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTatuajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tatuajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('mime');
            $table->string('fs_name');
            $table->integer('estilo_id')->unsigned();
            $table->integer('tatuador_id')->unsigned();

            $table->foreign('estilo_id')->references('id')->on('estilos');
            $table->foreign('tatuador_id')->references('id')->on('tatuadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tatuajes');
    }
}
