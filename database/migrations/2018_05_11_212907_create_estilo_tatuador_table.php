<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstiloTatuadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estilo_tatuador', function (Blueprint $table) {
          $table->integer('estilo_id')->unsigned();
          $table->foreign('estilo_id')->references('id')->on('estilos');
          $table->integer('tatuador_id')->unsigned();
          $table->foreign('tatuador_id')->references('id')->on('tatuadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estilo_tatuador');
    }
}
