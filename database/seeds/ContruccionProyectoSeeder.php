<?php

use Illuminate\Database\Seeder;

class ContruccionProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'solitoosink@gmail.com',
            'typeUser' => 'Administrador',
            'password' => bcrypt("123"),
        ]);
        DB::table('users')->insert([
            'name' => 'Tatuador',
            'email' => 'tatoosolitoos@gmail.com',
            'typeUser' => 'Tatuador',
            'password' => bcrypt("123"),
        ]);

        DB::table('tatuadores')->insert([
            'name' => 'Jorge Angel',
            'email' => 'hippie@gmail.com',
            'nick' => 'Hippie',
        ]);
        DB::table('tatuadores')->insert([
            'name' => 'Mario Humberto',
            'email' => 'Mayin@gmail.com',
            'nick' => 'Mayin',
        ]);

        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '1',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '2',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '3',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '4',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '5',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '6',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '7',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '8',
            'tatuador_id' => '1',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '9',
            'tatuador_id' => '1',
        ]);

        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '15',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '14',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '13',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '12',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '11',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '10',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '9',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '8',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '7',
            'tatuador_id' => '2',
        ]);
        DB::table('estilo_tatuador')->insert([
            'estilo_id' => '6',
            'tatuador_id' => '2',
        ]);
    }
}
