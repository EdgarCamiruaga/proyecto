<?php

use Illuminate\Database\Seeder;

class EstilosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('estilos')->insert([
      'nombre' => 'Realista',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Acuarela',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Tribal',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Graffiti',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Tradicional',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Neotradicional',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Góticos',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Escritos',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Geométricos',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Dotwork',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Japonés',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Flechas',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Glifos',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Números romanos',
      ]);
      DB::table('estilos')->insert([
      'nombre' => 'Corazones',
      ]);

    }
}
