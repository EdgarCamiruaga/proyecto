<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstilosSeeder::class);
        $this->call(ContruccionProyectoSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
